<div align="center">
    <h1>Ensembles Soundfonts</h1>
    <hr>
    Soundfonts for Ensembles Arranger Workstation app for elementary OS
    https://github.com/SubhadeepJasu/Ensembles
</div>
<br>

## Install from source
You can install Ensembles soundfonts using meson. Here's a list of dependencies:
 - `Soundfont editor like Polyphone`
 - `meson`
 - `Ensembles (optional)`

Clone repository and change directory
```
git clone https://gitlab.com/SubhadeepJasu/ensemblesgmsoundfont.git
cd ensemblesgmsoundfont
```
Compile and install Ensembles soundfonts on your system
```
meson _build --prefix=/usr
cd _build
sudo ninja install
```

## Discussions
If you want to ask any questions or provide feedback, you can make issues in this repository: https://github.com/SubhadeepJasu/Ensembles/issues

## Contributing
Feel free to send pull requests to this repository with your soundfont voices

## Large File Storage
You need git-lfs https://git-lfs.github.com/ in order to clone this repository properly. This repository may be recreated multiple times in future to remove old LFS history.